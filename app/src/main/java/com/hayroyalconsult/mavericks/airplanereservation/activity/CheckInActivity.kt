package com.hayroyalconsult.mavericks.airplanereservation.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.hayroyalconsult.mavericks.airplanereservation.R
import com.hayroyalconsult.mavericks.airplanereservation.helper.DbHelper
import com.hayroyalconsult.mavericks.airplanereservation.helper.ModelConverter
import com.hayroyalconsult.mavericks.airplanereservation.model.Booking
import com.hayroyalconsult.mavericks.airplanereservation.model.User
import com.tfb.fbtoast.FBToast

import kotlinx.android.synthetic.main.activity_check_in.*
import org.jetbrains.anko.indeterminateProgressDialog
import java.util.*
import java.util.concurrent.TimeUnit

class CheckInActivity : AppCompatActivity() {

    var  helper : DbHelper? = null
    var booking: Booking? = null
    var user : User? = null
    val TAG = "RESERVE-ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)
        setSupportActionBar(toolbar)

        supportActionBar?.title = "Reservation"
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        helper = DbHelper(this).open()
        user = ModelConverter.GsonToClass<User>(intent.getStringExtra("session"))
        checkin.setOnClickListener {
            when{
                res_edit.text.isNullOrEmpty() -> FBToast.errorToast(this,"Booking Number Field Is Empty", FBToast.LENGTH_SHORT)
                else -> {
                    booking = ModelConverter.conBooking(helper!!.getBookedByReservationNumber(res_edit.text.toString()))[0]
                    Log.e(TAG, booking.toString())
                    when {
                        booking == null -> FBToast.errorToast(this,"Unknown flight reservation, kindly Book A Flight", Toast.LENGTH_SHORT)
                        booking?.seat_number != null -> {
                            FBToast.errorToast(this, "You Have Benn Checked-In For This Flight", Toast.LENGTH_SHORT)
                            startActivity(Intent(this, DetailsActivity::class.java).apply {
                                putExtra("flight", Gson().toJson(booking?.flight))
                                putExtra("session", Gson().toJson(user))
                                putExtra("booking", "")
                            })
                            finish()
                        }
                        else -> {
                            val dialog = indeterminateProgressDialog(message = "Please wait a bit…", title = "Checking In...")

                            Thread(Runnable {
                                Log.e(TAG,"New Tag")
                                Thread.sleep(TimeUnit.SECONDS.toMillis(3))

                                when {
                                    helper?.updateBookingWithSeatNumber(booking!!.id,seatNumber())!! -> {
                                        runOnUiThread {
                                            FBToast.successToast(this, "Check-In Successful",Toast.LENGTH_SHORT)
                                            startActivity(Intent(this, DetailsActivity::class.java).apply {
                                                putExtra("flight", Gson().toJson(booking?.flight))
                                                putExtra("session", Gson().toJson(user))
                                                putExtra("booking", "")
                                            })
                                            finish()
                                        }
                                    }
                                    else -> {
                                        runOnUiThread {
                                            FBToast.errorToast(this, "Unable to reserve this booking", Toast.LENGTH_SHORT)
                                        }
                                    }
                                }
                                dialog.dismiss()
                            }).start()
                        }
                    }
                }
            }
        }

    }


    fun seatNumber():String{
        val letters = "ABCDEFGHJKLMNPQRSTUVWXYZ"
        var selected = "${letters[Random().nextInt(letters.length)]}${Random().nextInt(99)}"
        Log.e(TAG, selected)
        return selected
    }
}
