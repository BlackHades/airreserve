package com.hayroyalconsult.mavericks.airplanereservation.activity

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import com.hayroyalconsult.mavericks.airplanereservation.R
import com.hayroyalconsult.mavericks.airplanereservation.helper.DbHelper
import com.hayroyalconsult.mavericks.airplanereservation.helper.ModelConverter
import com.hayroyalconsult.mavericks.airplanereservation.model.Booking
import com.hayroyalconsult.mavericks.airplanereservation.model.Flight
import com.hayroyalconsult.mavericks.airplanereservation.model.User

import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.content_details.*
import org.jetbrains.anko.*
import org.jetbrains.anko.design.coordinatorLayout
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class DetailsActivity : AppCompatActivity() {

    var flight : Flight? = null
    var booking : Booking? = null
    var user : User? = null
    var helper:DbHelper? = null
    val TAG = "DETAILS ACTIVITY"
    var new_booking : Booking? = null
    var test : ArrayList<Booking>? = ArrayList()
    var booked_user : User? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        helper = DbHelper(this).open()
        user = ModelConverter.GsonToClass<User>(intent.getStringExtra("session"))
        //booking = ModelConverter.GsonToClass<Booking>(intent.getStringExtra("booking"))
        flight = ModelConverter.GsonToClass<Flight>(intent.getStringExtra("flight"))
        flight.let {
            merchant.text = "${merchant.text} ${flight!!.merchant}"
            departure.text = "${departure.text} ${flight!!.departure}"
            destination.text = "${destination.text} ${flight!!.destination}"
            amount.text = "${amount.text} NGN${flight!!.amount}"
            time_of_departure.text = "${time_of_departure.text} ${flight!!.timeOfDeparture}"
            journey_time.text = "${journey_time.text} ${flight!!.journeyTime}"
            status.text = "${status.text} ${flight!!.status}"
            getTest()


            book_btn.setOnClickListener {
                getTest()
                if(test!!.size ==  0) {
                    AlertDialog.Builder(this)
                            .setMessage("Are You Sure You Want To Book This Flight?")
                            .setTitle("Flight Booking")
                            .setPositiveButton("Yes") { dialog, _ ->
                                dialog.dismiss()
                                doBooking()
                            }
                            .setNegativeButton("No") { dialog, _ ->
                                dialog.dismiss()
                            }.show()
                } else{
                    Snackbar.make(container,"This Flight Has Already Been Booked", Snackbar.LENGTH_SHORT).show()
                }

            }

        }
    }

    private fun getTest() {
        if(user!!.role == 0){
            if(booking != null)
                test = ModelConverter.conBooking(helper!!.getBooked(booking!!.user_id, flight!!.id!!))
            book_btn.visibility = View.GONE
        }else
            test = ModelConverter.conBooking(helper!!.getBooked(user!!.id, flight!!.id!!))

        if(test!!.size > 0){
            book_btn.visibility = View.GONE
            val book = test!![0]
            Log.e(TAG, book.toString())

            if(book.bookNumber != null && book.bookNumber != 0){
                book_number.visibility = View.VISIBLE
                book_number.text = "Booking Number: ${book.bookNumber}"
            }

            if(book.reservationNumber != null && book.reservationNumber != 0){
                reservation_number.visibility = View.VISIBLE
                reservation_number.text = "Reservation Number: ${book.reservationNumber}"
            }
            if(book.seat_number != null){
                seat_number.visibility = View.VISIBLE
                seat_number.text = "Seat Number: ${book.seat_number} | ${book.status}"
            }else{

            }

            if(user!!.role == 0){
                user_email.visibility = View.VISIBLE
                booked_user = ModelConverter.conUser(helper!!.getUserById(booking!!.user_id))
                if(booked_user != null){
                    user_email.visibility = View.VISIBLE
                    user_email.text = "Booked By: ${booked_user!!.email}"
                }
            }
        }
    }

    fun doBooking(){
        val pb = ProgressDialog(this)
        pb.setMessage("Processing...")
        pb.show()
        Thread(Runnable {
            Thread.sleep(TimeUnit.SECONDS.toMillis(3))
            if(helper!!.addBooking(flight!!, user!!, generateRandomNumber())){
                new_booking = ModelConverter.booking(helper!!.getBooked(user!!.id, flight!!.id!!))
                Log.e(TAG, new_booking!!.toString())
                runOnUiThread {
                    Snackbar.make(container,"Flight Booked", Snackbar.LENGTH_SHORT).show()
                    pb.dismiss()
                    getTest()
                }
            }else{
                Snackbar.make(container,"Unable to book flight", Snackbar.LENGTH_SHORT).show()
            }
        }).start()

    }
    fun seatNumber():String{
        val letters = "ABCDEFGHJKLMNPQRSTUVWXYZ"
        var selected = "${letters[Random().nextInt(letters.length)]}${Random().nextInt(99)}"
        Log.e(TAG, selected)
        return selected
    }

    fun generateRandomNumber():Int{
        var random = Random().nextInt()
        random = if(random < 0) (random * -1) else random
        Log.e(TAG, random.toString())
        return random
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(user?.role == 0)
            menuInflater.inflate(R.menu.main, menu)
        else
            menuInflater.inflate(R.menu.user_menu, menu)

        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> {
                startActivity(Intent(this, SearchActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_bookings ->{
                startActivity(Intent(this, BookingsActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_reserve -> {
                startActivity(Intent(this, ReservationActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }

            R.id.action_checkin -> {
                startActivity(Intent(this, CheckInActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.logout -> {
                startActivity(Intent(this, SelectActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
