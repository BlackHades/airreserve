package com.hayroyalconsult.mavericks.airplanereservation.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ListView
import com.hayroyalconsult.mavericks.airplanereservation.R
import com.hayroyalconsult.mavericks.airplanereservation.adapter.MainAdapter
import com.hayroyalconsult.mavericks.airplanereservation.helper.DbHelper
import com.hayroyalconsult.mavericks.airplanereservation.helper.ModelConverter
import com.hayroyalconsult.mavericks.airplanereservation.model.Flight
import com.hayroyalconsult.mavericks.airplanereservation.model.User
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.widget.LinearLayout
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import com.google.gson.Gson


class MainActivity : AppCompatActivity(){

    var user : User? = null
    val TAG = "MainActivity"
    var navHeader : View? = null
    var adapter : MainAdapter? = null
    var flight_list : ArrayList<Flight>? = null
    var helper : DbHelper? = null
    var lv : ListView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        toolbar.title = "View Flights"
        supportActionBar?.title = "Flight List"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        fab.setOnClickListener {
            startActivity(Intent(this, AddActivity::class.java))
        }
        makeInitialization()
        user = ModelConverter.GsonToClass<User>(intent.getStringExtra("session"))
        Log.e(TAG, user.toString())
        makeRoleCheck()
    }

    private fun makeInitialization() {
       // navHeader = nav_view.getHeaderView(0)
        helper = DbHelper(this).open()
        lv = findViewById(R.id.flight_list)

    }

    override fun onResume() {
        super.onResume()
        adapterWork()
    }
    private fun adapterWork() {
        flight_list = ModelConverter.conListFlight(helper!!.getAllFlight())
        flight_list!!.reverse()
        adapter = MainAdapter(this,flight_list!!)
        if(flight_list!!.size > 0){
            lv!!.visibility = View.VISIBLE
            no_flight.visibility = View.GONE
            lv!!.adapter = adapter
            lv!!.setOnItemClickListener { _, _, position, _ ->
                val flight : Flight = lv!!.getItemAtPosition(position) as Flight
                flight.let{
                    if(flight.status == "OPEN"){
                        Log.e(TAG, flight.toString())
                        startActivity(Intent(this, DetailsActivity::class.java).apply {
                            putExtra("flight", Gson().toJson(flight))
                            putExtra("session", Gson().toJson(user))
                            putExtra("booking", "")
                        })
                        Log.e(TAG, flight.toString())
                    }else{
                        Snackbar.make(drawer_layout, "Flight Is Closed", Snackbar.LENGTH_SHORT).show()
                    }

                }

            }

            lv!!.setOnItemLongClickListener { _, _, position, _ ->
                val flight : Flight = lv!!.getItemAtPosition(position) as Flight
                if(user!!.role == 0){
                    val mBottomSheet = BottomSheetDialog(this)
                    val sheetView = layoutInflater.inflate(R.layout.bottom_menu, null)
                    mBottomSheet.setContentView(sheetView)
                    val data = sheetView.findViewById<LinearLayout>(R.id.delete)
                    data.setOnClickListener {
                        mBottomSheet.dismiss()
                        if(helper!!.deleteFlight(flight.id!!)){
                            flight_list!!.remove(flight)
                            flight.status = "CLOSED"
                            flight_list!!.add(flight)
                            Snackbar.make(drawer_layout,"Flight has been closed", Snackbar.LENGTH_SHORT).show()
                        } else Snackbar.make(drawer_layout,"Unable to close flight. Try Again", Snackbar.LENGTH_SHORT).show()
                        adapter!!.notifyDataSetChanged()
                    }
                    mBottomSheet.show()
                }
                return@setOnItemLongClickListener true
            }
        }else{
            lv!!.visibility = View.GONE
            no_flight.visibility = View.VISIBLE
        }
    }

    @SuppressLint("RestrictedApi")
    fun makeRoleCheck() {
        if(user?.role == 1){
            supportActionBar?.title = "Flight List (User)"
            fab.visibility = View.GONE
        }else{
            supportActionBar?.title = "Flight List (Admin)"
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(user?.role == 0)
            menuInflater.inflate(R.menu.main, menu)
        else
            menuInflater.inflate(R.menu.user_menu, menu)

        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> {
                startActivity(Intent(this, SearchActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_bookings ->{
                startActivity(Intent(this, BookingsActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_reserve -> {
                startActivity(Intent(this, ReservationActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }

            R.id.action_checkin -> {
                startActivity(Intent(this, CheckInActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.logout -> {
                startActivity(Intent(this, SelectActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
