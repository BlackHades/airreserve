package com.hayroyalconsult.mavericks.airplanereservation.activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.GONE
import android.widget.Toast
import com.google.gson.Gson
import com.hayroyalconsult.mavericks.airplanereservation.R
import com.hayroyalconsult.mavericks.airplanereservation.helper.DbHelper
import com.hayroyalconsult.mavericks.airplanereservation.helper.ModelConverter
import com.hayroyalconsult.mavericks.airplanereservation.model.Booking
import com.hayroyalconsult.mavericks.airplanereservation.model.User
import com.tfb.fbtoast.FBToast

import kotlinx.android.synthetic.main.activity_reservation.*
import org.jetbrains.anko.indeterminateProgressDialog
import java.util.*
import java.util.concurrent.TimeUnit

class ReservationActivity : AppCompatActivity() {

    var  helper : DbHelper? = null
    var booking: Booking? = null
    var user : User? = null
    val TAG = "RESERVE-ACTIVITY"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reservation)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "Reservation"
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        helper = DbHelper(this).open()
        user = ModelConverter.GsonToClass<User>(intent.getStringExtra("session"))

        reserve.setOnClickListener {
            when{
                booking_edit.text.isNullOrEmpty() -> FBToast.errorToast(this,"Booking Number Field Is Empty", FBToast.LENGTH_SHORT)
                else -> {
                    booking = ModelConverter.conBooking(helper!!.getBookedByBookingNumber(booking_edit.text.toString()))[0]
                    Log.e(TAG, booking.toString())
                    when{
                        booking == null -> FBToast.errorToast(this,"Unknown flight Boooking, kindly Book A Flight", Toast.LENGTH_SHORT)
                        booking?.reservationNumber != null && booking?.reservationNumber != 0 -> FBToast.errorToast(this,"This booking Number Has Been reserved", Toast.LENGTH_SHORT)
                        else -> {
                            pay_btn.visibility = View.VISIBLE
                        }
                    }
                }
            }
        }
        pay_btn.setOnClickListener {
            val dialog = indeterminateProgressDialog(message = "Please wait a bit…", title = "Paying...")

            Thread(Runnable {
                Log.e(TAG,"New Tag")
                Thread.sleep(TimeUnit.SECONDS.toMillis(3))

                when {
                    helper?.updateBookingWithReservationNumber(booking!!.id,generateRandomNumber())!! -> {
                        runOnUiThread {
                            FBToast.successToast(this, "Payment And Reservation Successful",Toast.LENGTH_SHORT)
                            pay_btn.visibility = GONE
                        }
                    }
                    else -> {
                        runOnUiThread {
                            FBToast.errorToast(this, "Unable to reserve this booking", Toast.LENGTH_SHORT)
                        }
                    }
                }
                dialog.dismiss()
            }).start()
        }
    }


    private fun generateRandomNumber():Int{
        var random = Random().nextInt()
        random = if(random < 0) (random * -1) else random
        Log.e(TAG, random.toString())
        return random
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(user?.role == 0)
            menuInflater.inflate(R.menu.main, menu)
        else
            menuInflater.inflate(R.menu.user_menu, menu)

        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> {
                startActivity(Intent(this, SearchActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_bookings ->{
                startActivity(Intent(this, BookingsActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }
            R.id.action_reserve -> true
            R.id.action_checkin -> {
                startActivity(Intent(this, CheckInActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                true
            }

            R.id.logout -> {
                startActivity(Intent(this, SelectActivity::class.java).apply {
                    putExtra("session", Gson().toJson(user))
                })
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
