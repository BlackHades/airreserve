package com.hayroyalconsult.mavericks.airplanereservation.activity

import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.hayroyalconsult.mavericks.airplanereservation.R
import com.hayroyalconsult.mavericks.airplanereservation.helper.DbHelper
import com.hayroyalconsult.mavericks.airplanereservation.helper.ModelConverter
import com.hayroyalconsult.mavericks.airplanereservation.model.User
import com.tfb.fbtoast.FBToast
import kotlinx.android.synthetic.main.activity_select.*
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.progressDialog
import java.util.concurrent.TimeUnit



class SelectActivity : AppCompatActivity() {

    var role : Int? = 0
    var pb : ProgressDialog? = null
    var TAG = "SelectActivity"
    var helper : DbHelper? = null
    var user : User? = null
    var type = "login"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select)
        helper = DbHelper(this)
        helper!!.open()
        pb = ProgressDialog(this)
        user = User();
        admin_sel.setOnClickListener {
            //input_email.setText("admin@admin.com")
            role = 0
            select_layout.visibility = View.GONE
            form_layout.visibility = View.VISIBLE
            radio_text.visibility = View.GONE
            radio_group.visibility = View.GONE
            type = "login"
            login_btn.text = "LOGIN"
        }
        user_sel.setOnClickListener {
            input_email.setText("user@user.com")
            role = 1
            select_layout.visibility = View.GONE
            form_layout.visibility = View.VISIBLE
            radio_text.visibility = View.GONE
            radio_group.visibility = View.GONE
            type = "login"
            login_btn.text = "LOGIN"
        }

        register.setOnClickListener {
            //input_email.setText("user@user.com")
            user?.role = if(radio_admin.isChecked)  0 else 1
            form_layout.visibility = View.VISIBLE
            radio_text.visibility = View.VISIBLE
            radio_group.visibility = View.VISIBLE
            select_layout.visibility = View.GONE
            type = "register"
            login_btn.text = "REGISTER"
        }

        login_btn.setOnClickListener {
            if(type == "register")
                doRegister()
            else
                doLogin()
        }

    }

    private fun doRegister() {
        if(input_email.text.isNullOrEmpty() || input_password.text.isNullOrEmpty())
            return FBToast.errorToast(this,"One or more field(s) is empty", Toast.LENGTH_SHORT)
        user?.email = input_email.text.toString()
        user?.password = input_password.text.toString()
        val dialog = indeterminateProgressDialog(message = "Please wait a bit…", title = "Registration")

        Thread(Runnable {
            Log.e(TAG,"New Tag")
            Thread.sleep(TimeUnit.SECONDS.toMillis(3))
            if(helper?.addUser(user)!!){
               this.runOnUiThread {
                   FBToast.successToast(this, "Registration Successful", Toast.LENGTH_SHORT)
                   if(user != null || user != User()){
                       startActivity(Intent(this, MainActivity::class.java).apply {
                           putExtra("session",Gson().toJson(user))
                       })
                       dialog.dismiss()
                   }else{
                       this.runOnUiThread {
                           input_email.error = "Invalid Credentials"
                           input_password.error = "Invalid Credential"
                           FBToast.errorToast(this,"An Error Occurred When Registering User", Toast.LENGTH_SHORT)
                           dialog.dismiss()
                       }
                   }
               }
            }else{
                this.runOnUiThread{
                    FBToast.errorToast(this,"An Error Occurred When Registering User", Toast.LENGTH_SHORT)
                }
            }
            dialog.dismiss()
        }).start()

    }

    private fun doLogin(){
        val email = input_email.text.toString()
        val password = input_password.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty()){
//            pb?.setMessage("Please Wait...")
//            pb?.show()
            Thread(Runnable {
                //Thread.sleep(TimeUnit.SECONDS.toMillis(3))
                user = ModelConverter.conUser(helper!!.getSpecificUser(email,password))
                Log.e(TAG , user.toString())
                pb?.dismiss()
                if(user != null){
                    startActivity(Intent(this, MainActivity::class.java).apply {
                        putExtra("session",Gson().toJson(user))
                    })
                }else{
                    this.runOnUiThread {
                        input_email.error = "Invalid Credentials"
                        input_password.error = "Invalid Credential"
                    }
                }
            }).start()
        }else{
            pb!!.dismiss()
            if(email.isEmpty()) input_email.error = "Cannot be empty"
            if(password.isEmpty()) input_password.error = "Cannot be empty"
        }
    }

    override fun onBackPressed() {
        if(form_layout.visibility == View.VISIBLE){
            select_layout.visibility = View.VISIBLE
            form_layout.visibility = View.GONE
        }else super.onBackPressed()
    }
    

}
